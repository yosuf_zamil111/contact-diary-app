package com.zamil.map.dairybook;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SendActivity extends AppCompatActivity {

    Button send;
    EditText sms,number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Intent intent= getIntent();
        final String MemID1 = intent.getStringExtra("MemID1");
        //final String MemID2 = intent.getStringExtra("MemID2");
       /* final String MemID3 = intent.getStringExtra("MemID3");
        final String MemID4 = intent.getStringExtra("MemID4");*/

        number = (EditText) findViewById(R.id.sendphone);
        sms = (EditText) findViewById(R.id.sendmessages);
        sms.setText(MemID1);
        //sms.setText(MemID2);
       /* sms.setText(MemID3);
        sms.setText(MemID4);
*/
        send = (Button) findViewById(R.id.sendsms);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("smsto:" + number.getText().toString());
                Intent smsSIntent = new Intent(Intent.ACTION_SENDTO, uri);
                // add the message at the sms_body extra field
                smsSIntent.putExtra("sms_body", sms.getText().toString());
                try {
                    startActivity(smsSIntent);
                } catch (Exception ex) {
                    Toast.makeText(SendActivity.this, "Your sms has failed...", Toast.LENGTH_LONG).show();
                    ex.printStackTrace();
                }
            }
        });



    }


}
