package com.zamil.map.dairybook;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsActivity extends AppCompatActivity {

    final myDBClass myDb = new myDBClass(this);
    Button cancel,update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Intent intent= getIntent();
        final String MemID = intent.getStringExtra("MemID");

        ShowData(MemID);

        cancel = (Button) findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(DetailsActivity.this, ListDeleteActivity.class);
                startActivity(out);
            }
        });
        update = (Button) findViewById(R.id.btnUpdate);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(DetailsActivity.this, EditActivity.class);
                out.putExtra("ID", MemID);
                startActivity(out);
            }
        });

    }

    public void calling(View v){
        try {
            //String phone = "+34666777888";

            Intent intent= getIntent();
            final String MemID = intent.getStringExtra("MemID");
            Intent intent1 = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", MemID, null));
            startActivity(intent1);

        }catch(Exception e) {
            Toast.makeText(getApplicationContext(), "Your call has failed...",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void ShowData(String MemID)
    {
        final TextView name = (TextView) findViewById(R.id.txtName);
        final Button phone = (Button) findViewById(R.id.txtPhone);
        final TextView email = (TextView) findViewById(R.id.txtEmail);
        final TextView address = (TextView) findViewById(R.id.txtAddress);
        final TextView birthday = (TextView) findViewById(R.id.txtBirthday);
        final TextView sex = (TextView) findViewById(R.id.txtSex);
        final TextView blood = (TextView) findViewById(R.id.txtBlood);

        final myDBClass myDb = new myDBClass(this);

        String arrData[] = myDb.SelectData(MemID);

        if(arrData != null)
        {
            name.setText(arrData[0]);
            phone.setText(arrData[1]);
            email.setText(arrData[2]);
            address.setText(arrData[3]);
            birthday.setText(arrData[4]);
            sex.setText(arrData[5]);
            blood.setText(arrData[6]);
        }
    }



}
