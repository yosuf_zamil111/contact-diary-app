package com.zamil.map.dairybook;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class ListDeleteActivity extends AppCompatActivity {

    ArrayList<HashMap<String, String>> MebmerList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_delete);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        ShowListData();
    }

    // Show List data
    public void ShowListData()
    {
        myDBClass myDb = new myDBClass(this);
        MebmerList = myDb.SelectAllData();
        ListView lisView1 = (ListView)findViewById(R.id.listView2);
        SimpleAdapter sAdap;
        sAdap = new SimpleAdapter(ListDeleteActivity.this, MebmerList, R.layout.activity_column1,
        new String[] {"Name", "Phone"}, new int[] {R.id.name1, R.id.phone1});
        lisView1.setAdapter(sAdap);
        registerForContextMenu(lisView1);
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        menu.setHeaderTitle("Command for : " + MebmerList.get(info.position).get("Name").toString());
        String[] menuItems = getResources().getStringArray(R.array.CmdMenu);
        for (int i = 0; i<menuItems.length; i++) {
            menu.add(Menu.NONE, i, i, menuItems[i]);
        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.CmdMenu);
        String CmdName = menuItems[menuItemIndex];
        String MemID = MebmerList.get(info.position).get("Phone").toString();
        if ("Edit".equals(CmdName)) {

            Intent out = new Intent(ListDeleteActivity.this,EditActivity.class);
            startActivity(out);

        }
        else if ("Delete".equals(CmdName)) {
            myDBClass myDb = new myDBClass(this);
            long flg = myDb.DeleteData(MemID);
            if(flg > 0)
            {
                Toast.makeText(ListDeleteActivity.this, "Delete Data Successfully", Toast.LENGTH_LONG).show();
                Intent out = new Intent(ListDeleteActivity.this,ShowActivity.class);
                startActivity(out);
            }
            else
            {
                Toast.makeText(ListDeleteActivity.this,"Delete Data Failed.", Toast.LENGTH_LONG).show();
            }
            ShowListData();
        }
        return true;
    }



}
