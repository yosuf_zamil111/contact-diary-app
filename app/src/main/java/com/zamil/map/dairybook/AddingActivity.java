package com.zamil.map.dairybook;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class AddingActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView,sex;
    private EditText name,phone,email,address;
    Spinner sp;
    private int year, month, day;

    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton1,radioSexButton2;
    String gender ="Male";
    Context ctx;
    Button save,cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ctx = this;
        radioSexGroup=(RadioGroup)findViewById(R.id.radioGroup);
        radioSexGroup.setOnCheckedChangeListener(this);
        radioSexButton1=(RadioButton)findViewById(R.id.radioButton1);
        radioSexButton2=(RadioButton)findViewById(R.id.radioButton2);

        dateView = (TextView) findViewById(R.id.day);
        calendar = Calendar.getInstance();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);

        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SaveData()) {


                    Toast.makeText(AddingActivity.this, "Add Data Successfully. ",
                            Toast.LENGTH_SHORT).show();

                    Intent newActivity = new Intent(AddingActivity.this, HomeActivity.class);
                    startActivity(newActivity);
                }
            }

        });

        sp = (Spinner) findViewById(R.id.spinner1);

        cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(AddingActivity.this,HomeActivity.class);
                startActivity(out);
            }
        });

    }

    public boolean SaveData()
    {
        final EditText name = (EditText) findViewById(R.id.name);
        final EditText phone = (EditText) findViewById(R.id.phone);
        final EditText email = (EditText) findViewById(R.id.email);
        final EditText address = (EditText) findViewById(R.id.address);

        final TextView dateView = (TextView) findViewById(R.id.day);

        sex = (TextView) findViewById(R.id.sex);
        sex.setText(gender);

        final TextView blood = (TextView) findViewById(R.id.blood);
        blood.setText(sp.getSelectedItem().toString());

        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        AlertDialog ad = adb.create();

        final myDBClass myDb = new myDBClass(this);


        long saveStatus = myDb.InsertData(
                name.getText().toString(),
                phone.getText().toString(),
                email.getText().toString(),
                address.getText().toString(),
                dateView.getText().toString(),
                sex.getText().toString(),
                blood.getText().toString()
        );
        if(name.getText().length() == 0)
        {
            ad.setMessage("Please input [Name] ");
            ad.show();
            name.requestFocus();
            return false;
        }
        if(phone.getText().length() == 0)
        {
            ad.setMessage("Please input [Phone Number] ");
            ad.show();
            phone.requestFocus();
            return false;
        }
        if(email.getText().length() == 0)
        {
            ad.setMessage("Please input [Email] ");
            ad.show();
            email.requestFocus();
            return false;
        }
        if(address.getText().length() == 0)
        {
            ad.setMessage("Please input [Address] ");
            ad.show();
            address.requestFocus();
            return false;
        }
        if(saveStatus <=  0)
        {
            ad.setMessage("Error!! ");
            ad.show();
            return false;
        }
        Toast.makeText(AddingActivity.this, "Add Data Successfully. ",
                Toast.LENGTH_SHORT).show();
        return true;
    }



    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        //Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
        }
    };
    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int position) {
        // TODO Auto-generated method stub
        switch (position) {
            case R.id.radioButton1:
                gender = "Male";
                //System.out.println("Male");
                break;
            case R.id.radioButton2:
                gender = "Female";
                //System.out.println("Female");
                break;

            default:
                break;
        }
    }


}
