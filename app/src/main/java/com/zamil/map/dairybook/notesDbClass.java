package com.zamil.map.dairybook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zamil on 31-Dec-15.
 */
public class notesDbClass extends SQLiteOpenHelper {


    public SQLiteDatabase db;


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "notes";
    private static final String TABLE_MEMBER = "mynote";

    public notesDbClass(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_MEMBER +
                " (Subject TEXT(100)," +
                /*" Phone TEXT(100)," +
                " Email TEXT(100)," +
                " Address TEXT(100)," +
                " Birthday TEXT(100)," +
                " Sex TEXT(100)," +*/
                " Body TEXT(700));");

        Log.d("CREATE TABLE", "Create Table Successfully.");
    }

    public long InsertData(String sub, String body) {
        try {
            SQLiteDatabase db;
            db = this.getWritableDatabase(); // Write Data
            ContentValues Val = new ContentValues();
            Val.put("Subject", sub);
            Val.put("Body", body);
           /* Val.put("Email", email);
            Val.put("Address", address);
            Val.put("Birthday", birth);
            Val.put("Sex", sex);
            Val.put("Blood", blood);*/

            long rows = db.insert(TABLE_MEMBER, null, Val);
            db.close();
            return rows; // return rows inserted.
        } catch (Exception e) {
            return -1;
        }
    }
    public ArrayList<HashMap<String, String>> SelectAllData() {
        try {
            ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;
            SQLiteDatabase db;
            db = this.getReadableDatabase(); // Read Data
            String strSQL = "SELECT  * FROM " + TABLE_MEMBER;
            Cursor cursor = db.rawQuery(strSQL, null);
            if(cursor != null)
            {
                if (cursor.moveToFirst()) {
                    do {
                        map = new HashMap<String, String>();
                        // map.put("MemberID", cursor.getString(0));
                        map.put("Subject", cursor.getString(0));
                        map.put("Body", cursor.getString(1));
                       /* map.put("Email", cursor.getString(2));
                        map.put("Address", cursor.getString(3));
                        map.put("Birthday", cursor.getString(4));
                        map.put("Sex", cursor.getString(5));
                        map.put("Blood", cursor.getString(1));*/
                        MyArrList.add(map);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();
            return MyArrList;
        } catch (Exception e) {
            return null;
        }
    }

    public long DeleteData(String strMemberID) {
        try {
            SQLiteDatabase db;
            db = this.getWritableDatabase(); // Write Data
            long rows = db.delete(TABLE_MEMBER, "Body = ?",
                    new String[] { String.valueOf(strMemberID) });
            db.close();
            return rows; // return rows deleted.
        } catch (Exception e) {
            return -1;
        }

    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMBER);
        onCreate(db);
    }
}
