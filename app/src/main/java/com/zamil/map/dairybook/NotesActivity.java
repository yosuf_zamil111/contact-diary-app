package com.zamil.map.dairybook;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class NotesActivity extends AppCompatActivity {

    //private EditText subject,message;
    Spinner spin;
    Button save,cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        spin = (Spinner) findViewById(R.id.spinnerNote);
        save = (Button) findViewById(R.id.addnote);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SaveData()) {
                    /*Toast.makeText(NotesActivity.this, "Note Successfully Saved. ",
                            Toast.LENGTH_SHORT).show();*/
                    Intent newActivity = new Intent(NotesActivity.this, ShowNotesActivity.class);
                    startActivity(newActivity);
                }
            }

        });
        cancel = (Button) findViewById(R.id.addcancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(NotesActivity.this,HomeActivity.class);
                startActivity(out);
            }
        });

    }
    public boolean SaveData()
    {
        final TextView sub = (TextView) findViewById(R.id.notes);
        sub.setText(spin.getSelectedItem().toString());

        final EditText messgaes = (EditText) findViewById(R.id.messages);



        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        AlertDialog ad = adb.create();

        final notesDbClass myDb = new notesDbClass(this);


        long saveStatus = myDb.InsertData(
                sub.getText().toString(),
                messgaes.getText().toString()
                /*email.getText().toString(),
                address.getText().toString(),
                dateView.getText().toString(),
                sex.getText().toString(),
                blood.getText().toString()*/
        );
        if(messgaes.getText().length() == 0)
        {
            ad.setMessage("Please input [Messages] ");
            ad.show();
            messgaes.requestFocus();
            return false;
        }
        if(saveStatus <=  0)
        {
            ad.setMessage("Error!! ");
            ad.show();
            return false;
        }
        Toast.makeText(NotesActivity.this, "Note Successfully Saved. ",
                Toast.LENGTH_SHORT).show();
        return true;
    }


}
