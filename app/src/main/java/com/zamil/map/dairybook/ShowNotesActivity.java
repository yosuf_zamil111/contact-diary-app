package com.zamil.map.dairybook;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class ShowNotesActivity extends AppCompatActivity {

    //final notesDbClass myDb = new notesDbClass(this);
    //ArrayList<HashMap<String, String>> MebmerList = myDb.SelectAllData();
    ArrayList<HashMap<String, String>> MebmerList1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_notes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final notesDbClass myDb = new notesDbClass(this);
        final ArrayList<HashMap<String, String>> MebmerList = myDb.SelectAllData();

        ListView lisView1 = (ListView)findViewById(R.id.listViewMsg);

        SimpleAdapter sAdap;

        sAdap = new SimpleAdapter(ShowNotesActivity.this, MebmerList, R.layout.show_notes,
                new String[] {"Subject","Body"},
                new int[] {R.id.sub,R.id.mess});

        lisView1.setAdapter(sAdap);

        ShowListData();

        //registerForContextMenu(lisView1);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        menu.setHeaderTitle("Command for : " + MebmerList1.get(info.position).get("Subject").toString());
        String[] menuItems = getResources().getStringArray(R.array.CmdMenu);
        for (int i = 0; i<menuItems.length; i++) {
            menu.add(Menu.NONE, i, i, menuItems[i]);
        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.CmdMenu);
        String CmdName = menuItems[menuItemIndex];
        String MemID = MebmerList1.get(info.position).get("Body").toString();
        if ("Edit".equals(CmdName)) {

            Intent out = new Intent(ShowNotesActivity.this,EditActivity.class);
            startActivity(out);

        }
        else if ("Delete".equals(CmdName)) {
            notesDbClass myDb = new notesDbClass(this);
            long flg = myDb.DeleteData(MemID);
            if(flg > 0)
            {
                Toast.makeText(ShowNotesActivity.this, "Delete Note Successfully", Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(ShowNotesActivity.this,"Delete Note Failed.", Toast.LENGTH_LONG).show();
            }
            ShowListData();
        }
        return true;
    }
    public void ShowListData()
    {
        notesDbClass myDb = new notesDbClass(this);

        //ArrayList<HashMap<String, String>> MebmerList1 = myDb.SelectAllData();
        MebmerList1 = myDb.SelectAllData();
        ListView lisView1 = (ListView)findViewById(R.id.listViewMsg);
        SimpleAdapter sAdap;
        sAdap = new SimpleAdapter(ShowNotesActivity.this, MebmerList1, R.layout.show_notes,
                new String[] {"Subject","Body"}, new int[] {R.id.sub, R.id.mess});
        lisView1.setAdapter(sAdap);
        registerForContextMenu(lisView1);
    }
}
