package com.zamil.map.dairybook;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.gsm.SmsManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class WishListActivity extends AppCompatActivity {

    TextView send1,send2,send3,send4,send5,send6,send7,send8,send9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        send1 = (TextView) findViewById(R.id.text1);
        send2 = (TextView) findViewById(R.id.text2);
        send3 = (TextView) findViewById(R.id.text3);
        send4 = (TextView) findViewById(R.id.text4);
        send5 = (TextView) findViewById(R.id.text5);
        send6 = (TextView) findViewById(R.id.text6);
        send7 = (TextView) findViewById(R.id.text7);
        send8 = (TextView) findViewById(R.id.text8);
        send9 = (TextView) findViewById(R.id.text9);



    }

    public void smssend(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send1.getText().toString());
        startActivity(out);

    }
    public void smssend1(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send2.getText().toString());
        startActivity(out);

    }

    public void smssend2(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send3.getText().toString());
        startActivity(out);

    }
    public void smssend3(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send4.getText().toString());
        startActivity(out);

    }
    public void smssend4(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send5.getText().toString());
        startActivity(out);

    }
    public void smssend5(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send6.getText().toString());
        startActivity(out);

    }
    public void smssend6(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send7.getText().toString());
        startActivity(out);

    }
    public void smssend7(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send8.getText().toString());
        startActivity(out);

    }
    public void smssend8(View v){

        Intent out = new Intent(WishListActivity.this,SendActivity.class);
        //out.putExtra("ID", MemID);
        out.putExtra("MemID1", send9.getText().toString());
        startActivity(out);

    }


}
