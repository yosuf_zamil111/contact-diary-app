package com.zamil.map.dairybook;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class EditActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView,tSex,tBlood,tPhone;
    private EditText name,phone,email,address;
    Spinner sp3;
    private int year, month, day;

    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton1,radioSexButton2;
    String gender ="Male";
    Context ctx;
    Button okButton,cancel;

    private EditText tName,tEmail,tAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Intent intent= getIntent();
        final String MemID = intent.getStringExtra("ID");

        sp3 = (Spinner) findViewById(R.id.spinner3);

        /*ctx = this;
        radioSexGroup=(RadioGroup)findViewById(R.id.radioGroup1);
        radioSexGroup.setOnCheckedChangeListener(this);
        radioSexButton1=(RadioButton)findViewById(R.id.radioButton2);
        radioSexButton2=(RadioButton)findViewById(R.id.radioButton3);*/

        dateView = (TextView) findViewById(R.id.txtDay);
        calendar = Calendar.getInstance();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);


        ShowData(MemID);

        cancel = (Button) findViewById(R.id.cancancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(EditActivity.this, ShowActivity.class);
                startActivity(out);
            }
        });

        okButton = (Button) findViewById(R.id.canUpdate);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UpdateData(MemID)) {
                    Intent out = new Intent(EditActivity.this, ShowActivity.class);
                    startActivity(out);
                }
            }
        });
    }
    public boolean UpdateData(String MemID)
    {
        tName = (EditText) findViewById(R.id.txtName);

        tPhone = (TextView) findViewById(R.id.txtPhone);
        tPhone.setText(getIntent().getStringExtra("MemID"));

        tEmail = (EditText) findViewById(R.id.txtEmail);
        tAddress = (EditText) findViewById(R.id.txtAddress);
        dateView = (TextView) findViewById(R.id.txtDay);

        //tSex = (TextView) findViewById(R.id.txtSex);
        //tSex.setText(gender);

        tBlood = (TextView) findViewById(R.id.txtBlood);
        tBlood.setText(sp3.getSelectedItem().toString());
       // String bld = tBlood.setText(sp.getSelectedItem().toString());

        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        AlertDialog ad = adb.create();

        /*if(tName.getText().length() == 0)
        {
            ad.setMessage("Please input [Desired Name] ");
            ad.show();
            tName.requestFocus();
            return false;
        }
        if(tPhone.getText().length() == 0)
        {
            ad.setMessage("Please input [Phone Number] ");
            ad.show();
            tPhone.requestFocus();
            return false;
        }
        if(tEmail.getText().length() == 0)
        {
            ad.setMessage("Please input [Email] ");
            ad.show();
            tEmail.requestFocus();
            return false;
        }
        if(tAddress.getText().length() == 0)
        {
            ad.setMessage("Please input [Address] ");
            ad.show();
            tAddress.requestFocus();
            return false;
        }*/
        final myDBClass myDb = new myDBClass(this);

        long saveStatus = myDb.UpdateData(
                tName.getText().toString(),
                MemID,
                //tPhone.getText().toString(),
                tEmail.getText().toString(),
                tAddress.getText().toString(),
                dateView.getText().toString(),
                //tSex.getText().toString(),
                tBlood.getText().toString()
        );

        if(saveStatus <=  0)
        {
            ad.setMessage("Error!! ");
            ad.show();
            return false;
        }

        Toast.makeText(EditActivity.this, "Update Data Successfully. ",
                Toast.LENGTH_SHORT).show();
        return true;
    }




    public void ShowData(String MemID)
    {
        final EditText tName = (EditText) findViewById(R.id.txtName);
        final TextView tPhone = (TextView) findViewById(R.id.txtPhone);
        final EditText tEmail = (EditText) findViewById(R.id.txtEmail);
        final EditText tAddress = (EditText) findViewById(R.id.txtAddress);

        final TextView tBirthday = (TextView) findViewById(R.id.txtDay);
        //final TextView tSex = (TextView) findViewById(R.id.txtSex);
        final TextView tBlood = (TextView) findViewById(R.id.txtBlood);


        final myDBClass myDb = new myDBClass(this);
        String arrData[] = myDb.SelectData(MemID);

        if(arrData != null)
        {
            tName.setText(arrData[0]);
            tPhone.setText(arrData[1]);
            tEmail.setText(arrData[2]);
            tAddress.setText(arrData[3]);
           /* tBirthday.setText(arrData[4]);
            tSex.setText(arrData[5]);
            tBlood.setText(arrData[6]);*/
        }
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        //Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
        }
    };
    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int position) {
        // TODO Auto-generated method stub
        switch (position) {
            case R.id.radioButton1:
                gender = "Male";
                //System.out.println("Male");
                break;
            case R.id.radioButton2:
                gender = "Female";
                //System.out.println("Female");
                break;

            default:
                break;
        }
    }


}
