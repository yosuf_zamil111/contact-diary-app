package com.zamil.map.dairybook;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }
    public void adding(View v){
        Intent out = new Intent(HomeActivity.this, AddingActivity.class);
        startActivity(out);
    }
    public void showList(View v){
        Intent out = new Intent(HomeActivity.this, ShowActivity.class);
        startActivity(out);
    }
    public void map(View v){
        Intent out = new Intent(HomeActivity.this, MapsActivity.class);
        startActivity(out);
    }

    public void notes(View v){
        Intent out = new Intent(HomeActivity.this, NotesActivity.class);
        startActivity(out);
    }
    public void about(View v){
        Intent out = new Intent(HomeActivity.this, AboutActivity.class);
        startActivity(out);
    }
    public void wish(View v){
        Intent out = new Intent(HomeActivity.this, WishListActivity.class);
        startActivity(out);
    }
}
